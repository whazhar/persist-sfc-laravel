<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class OrderController extends Controller
{
    public function show($orderNumber)
    {
        $orderInfo = DB::select('select * from DWH_Activities where order_num = ?', [$orderNumber]);

        return $orderInfo;
    }
}
