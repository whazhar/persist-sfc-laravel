<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class ServiceController extends Controller
{
    public function show($serviceNumber)
    {
        $serviceInfo = DB::select('select * from DWH_Activities where service_num = ?', [$serviceNumber]);
        
        return $serviceInfo;
    }
}
